package tp1;

public class Compte {

	//Attributs

	private int numero, solde;
	private double decouvert;

	//Constructeurs

	Compte(int numero){
		this.numero=numero;
		decouvert=0;
		solde=0;

	}

	//Accesseurs

	void setDecouvert(double montant) {
		decouvert=montant;
	}
	double getDecouvert() {
		return decouvert;
	}
	int getNumero() {
		return numero;
	}
	double getSolde() {
		return solde;
	}

	public void afficherSolde(){
		System.out.println("Votre Solde est "+ getSolde());
	}
	//Methodes
	
	void depot(double montant) {
		solde+=montant;
	}
	String retrait(double montant) {
		if(montant>decouvert+solde) {
		System.out.println("Refuser");
		
	}
		else {
			solde-=montant;
			System.out.println("Accepter");
		}
	return null;
	}
}
    
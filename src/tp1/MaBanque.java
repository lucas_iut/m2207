package tp1;

public class MaBanque {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Compte p, c2;
		p=new Compte(1);
		p.afficherSolde();
		System.out.println(p.getDecouvert());
		p.setDecouvert(100);
		System.out.println(p.getDecouvert());
		p.depot(50);
		p.afficherSolde();
		p.retrait(150);
		p.afficherSolde();
		
		//Exercice2.6
		
		c2=new Compte(2);
		c2.setDecouvert(100);
		c2.depot(1000);
		c2.afficherSolde();
		c2.retrait(600);
		c2.afficherSolde();
		c2.retrait(700);
		c2.afficherSolde();
		c2.setDecouvert(500);
		c2.retrait(700);
		c2.afficherSolde();
	}
		
}

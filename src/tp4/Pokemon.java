package tp4;

public class Pokemon {

	//Attributs

	private int energie, maxEnergie;
	String nom;
	private int puissance;
	

	//Constructeurs

	Pokemon(String n){
		nom=n;
		maxEnergie= 50 + (int) (Math.random() * ((90 - 50) + 1));
		energie=1 + (int) (Math.random() * ((maxEnergie - 1) + 1));
		puissance=3 + (int) (Math.random() * ((10 - 3) + 1));
	}


	//Accesseurs

	String getNom() {
		return nom;
	}

	int getEnergie() {
		return energie;
	}
	
	int getPuissance() {
		return puissance;
	}

	//M�thodes

	void sePresenter() {
		System.out.println("Je suis "+nom+", j'ai "+energie+" points d'energie "+"("+maxEnergie+") et j'ai "+puissance+" point de puissance");		
	}

	void manger(){
		if(energie<maxEnergie && energie >0) 
		{ 
			energie+= 10 + (int) (Math.random() * ((30 - 10) + 1));
			if(energie>maxEnergie) 
			{
				energie=maxEnergie;
			}
			else 
			{

			}

		}
		else 
		{
			energie=maxEnergie;
		}
				}
	
	void vivre(){
		energie-=20 + (int) (Math.random() * ((40 - 20) + 1));
		if(energie <0) {
			energie =0;
		}
				}
	boolean isAlive() {
		
		if(energie==0) {
			return false;
		}
		return true;
	}
	void perdreEnergie(int perte) {
		energie-=perte;
	}
	void attaquer(Pokemon adversaire) {
		adversaire.energie-=puissance;
	}
}


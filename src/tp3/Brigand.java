package tp3;

public class Brigand extends Humain{
	
	//Attributs
	
	private String look;
	private int nbKidnaping;
	private int recompense;
	private boolean prison;
	
	//Constructeurs
	
	public Brigand(String n) {
		super(n);
		look="m�chant";
		prison=false;
		recompense=100;
		nbKidnaping=0;
		boisson="cognac";
	}
	//M�thodes
		
	int getRecompense() {
		return recompense;
	}
		
	String quelEstTonNom() {
		return  nom+" le "+look;
	}
	void sePresenter() {
		super.sePresenter();
		parler(" J'ai l'air "+look+" et j'ai enleve "+nbKidnaping+" dames");
		parler("Ma t�te est mise a prix "+recompense+" $");
	}
	void enleve(Dame dame){
		
		dame.etat=false;
		nbKidnaping=+1;
		recompense=+100;
		dame.parler("Au secours");
		parler("Ah ah ! ("+dame.nom+"), tu es ma prisonni�re !");
		
	}
	void emprisonner(Sherif sherif) {
		parler("Damned, je suis fait ! "+sherif.nom+", tu m'as eu");
	}
	
	
	
}
  
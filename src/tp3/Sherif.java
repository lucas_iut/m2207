package tp3;

public class Sherif extends Cowboy {
	
	//Attributs
	
	protected int arrestation;
	
	public Sherif(String n){
		super(n);
		nom=n;
		arrestation=0;
	}
	
	
	String quelEstTonNom() {
		return "Sh�rif "+nom;
	}
	
	void sePresenter() {
		parler("Bonjour je suis Sh�rif "+nom+" Je suis le meilleur, j'ai arr�t� "+arrestation+" brigands");
	}
	
	void coffrer(Brigand brigand) {
		arrestation+=1;
		parler("Au nom de la loi je vous arr�te, "+brigand.nom);
	}

}

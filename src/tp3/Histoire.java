package tp3;

public class Histoire {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Humain bob;
		bob=new Humain("bob");
		bob.parler("");
		bob.sePresenter();
		bob.boire();
		
		Dame Rachel;
		Rachel=new Dame("Rachel");
		Rachel.sePresenter();
		Rachel.priseEnOtage();
		Rachel.estLiberee();
		
		Brigand Gunther;
		Gunther=new Brigand("Gunther");
		Gunther.sePresenter();
		
		Cowboy Simon;
		Simon=new Cowboy("Simon");
		Simon.sePresenter();
		
		Gunther.enleve(Rachel);
		Rachel.sePresenter();
		Simon.tire(Gunther);
		Simon.libere(Rachel);
		Rachel.sePresenter();
		
		Sherif Max;
		Max=new Sherif("Max");
		Max.sePresenter();
		
	}

}

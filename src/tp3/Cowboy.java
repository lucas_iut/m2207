package tp3;

public class Cowboy extends Humain {

	//Attributs
	
	private int popularite;
	private String caracteristique;
	
	
	
	//Constructeurs
	public Cowboy(String n) {
		super(n);
		nom=n;
		boisson="Whiskey";
		popularite = 0;
		caracteristique = "vaillant";
	}
	//Methodes
	
	void tire(Brigand brigand) {
		parler("Le "+caracteristique+" "+nom+" tire sur "+brigand.nom+". PAN !");
		parler("Prend �a, voyou !");
	}
	
	void libere(Dame dame) {
		dame.etat=true;
		popularite+=10;
		dame.parler("Merci cowboy");
	}
	
	
}

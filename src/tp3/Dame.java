package tp3;

public class Dame extends Humain {
	
	//Attributs
	
	protected boolean etat;

	
	//Constructeurs
	
	public Dame(String n) {
		super(n);
		nom=n;
		boisson="Martini";
		etat= true;
	}
	
	//M�thodes
	
	void priseEnOtage() {
		etat =false;
		parler("Au secours !");
	}
	
	void estLiberee() {
		etat = true;
		parler("Merci Cowboy");
	}
	
	String quelEstTonNom() {
		return "Miss "+nom;
	}
	void sePresenter() {
		super.sePresenter();
		parler("Actuellement je suis "+etat); 
		
	}
	
}	


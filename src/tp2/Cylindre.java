package tp2;

public class Cylindre extends Cercle {


	//Attributs

	double hauteur;

	//Constructeurs

	Cylindre(){
		super();
	hauteur =1;
	}
    
	Cylindre(double h, double rayon, String couleur, boolean coloriage){
	    super(rayon,couleur,coloriage);
	    hauteur=h;
	
	}
	
	
	
	//Acesseurs
	
	double getHauteur() {
		return hauteur;
	}
	
	void setHauteur(double h) {
		this.hauteur=h;
	}
	
	//Methodes
	
	String seDecrire() {
		return "un cylindre de rayon "+getRayon()+" d'une couleur "+getCouleur()+", de coloriage "+isColoriage()+" et d'une hauteur "+getHauteur();
	}
	
	double calculerVolume() {
		return (getRayon()*getRayon())*hauteur*Math.PI;
		
	}

}

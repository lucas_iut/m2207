package tp2;

public class TestForme {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Forme f1,f2;
		f1=new Forme();
		
		f2=new Forme("vert", false);
		
		System.out.println(f1.getCouleur()+f1.isColoriage());
		System.out.println(f2.getCouleur()+f2.isColoriage());
		
		f1.setCouleur("rouge");
		f1.setColoriage(false);
		System.out.println(f1.getCouleur()+f1.isColoriage());
		System.out.println(f1.seDecrire());
		System.out.println(f2.seDecrire());
		
		Cercle c1;
		c1 =new Cercle();
		System.out.println("Un Cercle de rayon "+c1.getRayon()+" est issue d'une Forme de couleur "+c1.getCouleur()+" et de coloriage "+c1.isColoriage() );
		System.out.println(c1.seDecrire());
		
		Cercle c2;
		c2 =new Cercle(2.5);
		System.out.println(c2.seDecrire());
		
		Cercle c3;
		c3 =new Cercle(3.2, "jaune",false);
		System.out.println(c3.seDecrire());
		
		
		System.out.println(c3.calculerAire());
		System.out.println(c3.calculerPerimetre());
		
		System.out.println(c2.calculerAire());
		System.out.println(c2.calculerPerimetre());
		
		Cylindre cy1,cy2;
		cy1 =new Cylindre();
		System.out.println(cy1.seDecrire());
		
		cy2 = new Cylindre(4.2,1.3,"bleu",true);
		System.out.println(cy2.seDecrire());
		
		System.out.println("Le Volume de cy1 est  "+ cy1.calculerVolume());
		System.out.println("Le Volume de cy2 est  "+ cy2.calculerVolume());
		
	}

}

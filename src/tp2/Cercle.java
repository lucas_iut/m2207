package tp2;

public class Cercle extends Forme{

		//Attributs
		public double rayon;
		
		
		//Constructeurs
		Cercle(){
		super();	
		 this.rayon=1;
		}
		Cercle(double r){
			super();
			rayon=r;
		}
		Cercle(double r, String couleur,boolean coloriage){
			super(couleur, coloriage);
			rayon=r;
		}
		
		
		//Acesseurs	
		
		
		double getRayon() {
			return rayon;
		}
		
		void setRayon(double r) {
			this.rayon=r;
		}
		
		//M�thodes
		String seDecrire() {
			return "un cercle de rayon "+rayon+" d'une forme de couleur "+couleur+" et de coloriage "+coloriage;
			
		}
		double calculerAire() {
			return Math.PI*rayon*rayon;
		}
		double calculerPerimetre() {
			return 2*Math.PI*rayon;
		}
		
		
}
